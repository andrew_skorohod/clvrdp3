from django.shortcuts import render,redirect
from .models import Profile
from django.contrib.auth.models import User
# Create your views here.

def main(request):
    profiles= Profile.objects.all()
    context = {
        'profiles':profiles,
    }
    return render(request, 'home.html',context)


def profile_show(request,id):
    profile = Profile.objects.get(pk=id)#pk is primary key
    context = {
        'profile':profile,
    }
    return render (request,'profile.html',context)


def profile_edit(request,id):
    profile = Profile.objects.get(pk=id)
    context = {
        'profile':profile,
    }
    return render(request, 'edit.html', context)

def profile_save(request,id):
    profile = Profile.objects.get(pk=id)
    if request.POST.get('birthday'):
        profile.birthday = request.POST.get('birthday')
    if request.POST.get('username'):
        user = profile.user#
        user.username = request.POST.get('username')#changing user that is different model
        user.save()
    if request.POST.get('city'):
        profile.city = request.POST.get('city')
    profile.save()
    return redirect("/")