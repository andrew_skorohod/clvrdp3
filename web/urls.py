from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$',views.main,name="main"),
    url(r'^profile/(?P<id>\d+)/$', views.profile_show),
    url(r'^profile/edit/(?P<id>\d+)/$', views.profile_edit),
    url(r'^profile/save/(?P<id>\d+)/$', views.profile_save),
]