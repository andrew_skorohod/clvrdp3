from django.contrib import admin
from .models import Profile
# Register your models here.

#adding profiles only awailable in admin
admin.site.register(Profile)